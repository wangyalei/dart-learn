/**
 * <p>program: dart-learn</p>
 * <p>Description: 继承处理</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main() {
  Animal animal = Dog('black');
  animal.eat();
  (animal as Dog).bark();

  if (animal is Duck) {
    print('Yes');
  } else {
    print('No');
  }

  Duck duck = Duck('white');
  duck.eat();
  print(duck is Animal);

  YellowFlyDuck yellowFlyDuck = YellowFlyDuck();
  yellowFlyDuck.eat();
  yellowFlyDuck.flyInSky();
  yellowFlyDuck.count();
  yellowFlyDuck.output();

  // print(YellowFlyDuck.type);
}

class Animal {
  String color;

  eat() {
    print('Eat!');
  }
}

class Dog extends Animal {
  Dog(String color) {
    super.color = color;
  }

  @override
  eat() {
    print('$color dog eats meat.');
  }

  void bark() {
    print("Bark !");
  }
}

class Duck extends Animal {
  String type = "DUCK";

  Duck(String color) {
    super.color = color;
  }

  @override
  eat() {
    print('$color duck eats rice.');
  }
}

abstract class Fly {
  void flyInSky();
}

class CountableMixin {
  int _count = 0;

  void count() {
    _count++;
  }

  output() {
    print('count: $_count');
  }
}


class YellowFlyDuck extends Duck with CountableMixin implements Fly {
  // 父类构造有参数的情况 /或命名的参数 super.withName()
  YellowFlyDuck() : super('Yellow');

  @override
  void flyInSky() {
    print('$color duck Fly!');
  }
}
