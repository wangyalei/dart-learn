/**
 * <p>program: dart-learn</p>
 * <p>Description: conditional expressions 常用条件</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */

void main(){

  var marks = 70;
  if (marks >= 90 && marks < 100) {
    print("A+ grade");
  } else if (marks >= 80 && marks < 90) {
    print("A grade");
  } else if (marks >= 70 && marks < 80) {
    print("B grade");
  } else if (marks >= 60 && marks < 70) {
    print("C grade");
  } else if (marks > 30 && marks < 60) {
    print("D grade");
  } else if (marks >= 0 && marks < 30) {
    print("You have failed");
  } else {
    print("Invalid Marks. Please try again !");
  }

  int a = 2;
  int b = 3;
  int smallNumber = a < b ? a : b;
  print("$smallNumber is smaller");

  // 条件表达式
  String name;
  print(name ?? "Guest User");

  String grade = 'A';
  switch (grade) {
    case 'A':
      print("Excellent grade of A");
      break;
    case 'B':
      print("Very Good !");
      break;
    case 'C':
      print("Good enough. But work hard");
      break;
    case 'F':
      print("You have failed");
      break;
    default:
      print("Invalid Grade");
  }
}