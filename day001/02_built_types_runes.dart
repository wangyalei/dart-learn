/**
 * <p>program: dart-learn</p>
 * <p>Description: 内置数据类型
 * numbers(int double)
 * strings
 * booleans
 * lists (arrays)
 * maps
 * runes(为表达Unicode字符)
 * symbols
 * </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main(List<String> args) {

  //  1.在Dart中，Runes是采用UTF-32字符编码的字符串。

  var clapping = '\u{1f44f}';
  print(clapping);
  print(clapping.codeUnits);
  print(clapping.runes.toList());

  Runes input = new Runes(
      '\u2665  \u{1f605}  \u{1f60e}  \u{1f47b}  \u{1f596}  \u{1f44d}');
  print(new String.fromCharCodes(input));
}
