/**
 * <p>program: dart-learn</p>
 * <p>Description: 内置数据类型
 * numbers(int double)
 * strings
 * booleans
 * lists (arrays)
 * maps
 * runes(为表达Unicode字符)
 * symbols
 * </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main(List<String> args){

 // dart的列表（list）常量跟js的集合（array）常量有点类似：

  // 创建一个int类型的list
  List list = [10, 7, 23];
  print(list); // 输出结果  [10, 7, 23]
  // 添加元素
  list.add(1);
  // 删除元素
  list.remove(10);
  // 查找
  // int indexOf(E element, [int start = 0]);
  print(list.indexOf(1, 8)); // -1
  print(list); // [7, 23, 1]
  // 包含
  print(list.contains(100));
  // list转map
  print(list.asMap());

  // 要创建一个编译时常量const的list，示例如下：
  List constantList = const [10, 3, 15];
  print(constantList); // 输出结果  [10, 3, 15]

}