/**
 * <p>program: dart-learn</p>
 * <p>Description: 内置数据类型
 * numbers(int double)
 * strings
 * booleans
 * lists (arrays)
 * maps
 * runes(为表达Unicode字符)
 * symbols
 * </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main(List<String> args){

//  int和double都是num的子类。num类包含基础的操作符+ - / *，也支持abs(),ceil()和floor()
//  其中，位运算>>是定义在int中，如果num和及其子类没有你想要的方法，那么可以使用dart:math库

  var num1 = 100; // type 'int'
  int num2 = 200;
  double num3 = 30.0; // dart语言没有float关键字，使用double

  print("Numbers: num1=$num1, num2=$num2, num3=$num3, num1+num2=${num1 + num2}");
  // 'var' 定义的变量首次赋值后类型就不可以改变了, num1为'int'类型
  double num4 = 100;
  // print(num1 = num4 as int);// type 'double' is not a subtype of type 'int' in type cast

  // Dart2.1,double也有api转成int,会把小数点后面的全部去掉
  print(num1 == num4.toInt());

  double test2 = 15.1;
  double test3 = 15.1234;
  print(test2.toInt()); // 结果是15
  print(test3.toInt()); // 结果是15

  int number; // 未赋值的变量默认值为'null'
  print(number == null ? 'null' : 'not null');

  number = 123;
  dynamic obj = number;// dynamic 与 java中的 Object 类似
  print("obj is 'int', value is $obj");
  obj = "Hello";
  print("obj is 'String', value is $obj");

  // String转number的例子
  var one = int.parse('1');
  assert(one == 1);
  // String -> double
  var onePointOne = double.parse('1.1');
  assert(onePointOne == 1.1);
  // int -> String
  String oneAsString = 1.toString();
  assert(oneAsString == '1');
  // double -> String
  String piAsString = 3.14159.toStringAsFixed(2);
  assert(piAsString == '3.14');





  // 常量
  final langName = 'Dart';
  // langName = 'Java'; [Error]  a final variable, can only be set once.
  // const
  const PI = 3.14;
  print('$langName, $PI');

  /****
   * const和final
   *  相同点:必须初始化，初始化后都是只读的，不可变
   *  不同点:const表示编译时常量，即在代码还没有运行时我们就知道它声明变量的值是什么；
   *  而final不仅有const的编译时常量的特性，最重要的它是运行时常量，
   *  并且final是惰性初始化，即在运行时第一次使用前才初始化
   *  final x = new DateTime.now(); // 正确
      const x = new DateTime.now(); // 错误
   */

  // 要创建一个编译时常量const的map，请在map文字之前添加const
  final fruitConstantMap = const {2: 'apple', 10: 'orange', 18: 'banana'};
  // 打印结果{second: 腾讯, fifth: 百度, 5: 华为}
  print(fruitConstantMap);

}

class Circle {
  //const PI = 3.14; [Error] Only static fields can be declared as const.
  // 常量必须在定义时赋值
  static const PI = 3.14;
  // final 的变量可以在构造函数中赋值
  final double radius;
  Circle(this.radius);
}