/**
 * <p>program: dart-learn</p>
 * <p>Description: 内置数据类型-  字符串的拼接
 * numbers(int double)
 * strings
 * booleans
 * lists (arrays)
 * maps
 * runes(为表达Unicode字符)
 * symbols
 * </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main(List<String> args){

  // Dart字符串采用的是UTF-16字符编码。使用单引号或双引号来创建字符串：

  /***
   *
   * 单引号或者双引号
   */
  String str1 = '单引号基本使用demo.';
  print(str1);

  String str2 = "双引号基本使用demo.";
  print(str2);

  /***
   * 单引号或者双引号里面嵌套使用引号
   * 单引号里面嵌套单引号，或者//双引号里面嵌套双引号，必须在前面加反斜杠
   */
  // 单引号里面有单引号，必须在前面加反斜杠
  String str3 = '单引号里面有单引号it\'s，必须在前面加反斜杠.';
  print(str3);
  // 双引号里面嵌套单引号（正常使用）
  String str4 = "双引号里面有单引号it's.";
  print(str4);
  // 单引号里面嵌套双引号（正常使用）
  String str5 = '单引号里面有双引号，"hello world"';
  print(str5);
  // 双引号里面嵌套双引号，必须在前面加反斜杠
  String str6 = "双引号里面有双引号，\"hello world\"";
  print(str6);

  /***
   * 多个字符串相邻中间的空格问题
   * 双引号里面有单引号，混合使用运行正常
   */
  // 这个会报错
  //String blankStr1 = 'hello''''world';

  // 这两个运行正常
  String blankStr2 = 'hello'' ''world'; //结果： hello world
  String blankStr3 = 'hello''_''world'; //结果： hello_world

  // 单引号里面有双引号，混合使用运行正常
  String blankStr7 = 'hello""""world'; //结果： hello""""world
  String blankStr8 = 'hello"" ""world'; //结果： hello"" ""world
  String blankStr9 = 'hello""_""world'; //结果： hello""_""world

  // 双引号里面有单引号，混合使用运行正常
  String blankStr10 = "hello''''world"; //结果： hello''''world
  String blankStr11 = "hello'' ''world"; //结果： hello'' ''world
  String blankStr12 = "hello''_''world"; //结果： hello''_''world


  String connectionStr1 =  '字符串连接''甚至可以在'
      '换行的时候进行。';
  print(connectionStr1); // 字符串连接甚至可以在换行的时候进行

  String connectionStr2 =  '字符串连接'+ '甚至可以在'+ '换行的时候进行。';
  print(connectionStr2); // 字符串连接甚至可以在换行的时候进行

  /***
   * 使用单引号或双引号的三引号：
   */
  String connectionStr3 = ''' 
  这是用单引号创建的
  多行字符串。
  ''' ;
  print(connectionStr3);
  String connectionStr4 = """这是用双引号创建的
  多行字符串。""";
  print(connectionStr4);

  /****
   * 关于转义符号的使用  声明raw字符串（前缀为r），
   *    在字符串前加字符r，或者在\前面再加一个\，
   */
  print(r"换行符：\n"); //这个结果是 换行符：\n
  print("换行符：\\n"); //这个结果是 换行符：\n
  print("换行符：\n" "dart");  //这个结果是 自动换行
}