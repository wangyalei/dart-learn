/**
 * <p>program: dart-learn</p>
 * <p>Description: dart 注释规范及引用用法</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main(){

  // 注释
  var lang = "Dart";

  /**
   * 多行
   * 注释
   */
  print("Hello $lang World"); // 单行注释

  ///
  /// 多行
  /// 注释
  ///
  print("This is my first $lang application");

}