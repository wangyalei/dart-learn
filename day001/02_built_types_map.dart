/**
 * <p>program: dart-learn</p>
 * <p>Description: 内置数据类型
 * numbers(int double)
 * strings
 * booleans
 * lists (arrays)
 * maps
 * runes(为表达Unicode字符)
 * symbols
 * </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main(List<String> args) {

  //  1.创建map有两种方式。
  //  2.map的key类型不一致也不会报错。
  //  3.添加元素的时候，会按照你添加元素的顺序逐个加入到map里面，哪怕你的key不连续。
  //  比如key分别是 1,2,4，看起来有间隔，事实上添加到map的时候{1:value,2:value,4:value} 这种形式。
  //  4.添加的元素的key如果是map里面某个key的英文，照样可以添加到map里面，
  //  比如可以为3和key为three可以同时存在。
  //  5.map里面的key不能相同，但是value可以相同,value可以为空字符串或者为null。

  // 直接声明，用{}表示，里面写key和value，每组键值对中间用逗号隔开。
  Map companies = {'first': '阿里巴巴', 'second': '腾讯', 'fifth': '百度'};
  print(companies); // 打印结果 {first: 阿里巴巴, second: 腾讯, fifth: 百度}
  Map companies01 = new Map();
  companies01['first'] = '阿里巴巴';
  companies01['second'] = '腾讯';
  companies01['fifth'] = '百度';
  print(companies01);

  // 添加元素。格式: 变量名[key] = value,其中key可以是不同类型

  //添加一个新的元素，key为“5”，value为“华为”
  companies01[5] = '华为';
  print(companies01); // 打印结果 {first: 阿里巴巴, second: 腾讯, fifth: 百度, 5: 华为}
  // 修改元素。格式：变量名[key] = value
  companies01['first'] = 'alibaba';
  print(companies01); //打印结果 {first: alibaba, second: 腾讯, fifth: 百度, 5: 华为}
  // 包含
  bool mapKey = companies01.containsKey('second');
  bool mapValue = companies01.containsValue('百度');
  print(mapKey); //结果为：true
  print(mapValue); //结果为：true
  // 删除元素
  companies01.remove('first'); // 移除key为“first”的元素。
  print(companies01); // 打印结果{second: 腾讯, fifth: 百度, 5: 华为}
  companies01.clear(); // 清空map集合的数据。
  print(companies01); // 打印结果{}
  // 使用.length来获取map中键值对的数量：
  print(companies01.length);
  // 要创建一个编译时常量const的map，请在map文字之前添加const
  final fruitConstantMap = const {2: 'apple', 10: 'orange', 18: 'banana'};
  // 打印结果{second: 腾讯, fifth: 百度, 5: 华为}
  print(fruitConstantMap);
}
