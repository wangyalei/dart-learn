/**
 * <p>program: dart-learn</p>
 * <p>Description: 异常处理</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main(){

  try {
    int result = 12 ~/ 0;
    print("The result is $result");
  } on IntegerDivisionByZeroException {
    print('Cannot divide by Zero');
  }

  try {
    int result = 12 ~/ 0;
    print("The result is $result");
  } catch (e) {
    print("The exception thrown is $e");
  } finally {
    print("This is FINALLY Clause and is always executed.");
  }

  // 自定义异常
  try {
    throwFunc();
  } catch (e) {
    print(e);
  }
}

throwFunc() {
  throw CustomException();
}

class CustomException implements Exception {
  @override
  String toString() {
    return "This is Custom Exception.";
  }
}