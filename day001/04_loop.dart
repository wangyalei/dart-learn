/**
 * <p>program: dart-learn</p>
 * <p>Description:控制语句 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
main(){

  for (int i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      print(i);
    }
  }

  List planetList = ["Mercury", "Venus", "Earth", "Mars"];
  for (String planet in planetList) {
    print(planet);
  }

  var i = 1;
  while (i <= 10) {
    if (i % 2 == 0) {
      print(i);
    }
    i++;
  }

  do {
    i--;
    if (i < 5) {
      print(i);
    }
  } while (i > 0);

  // break label跳到标签出并且结束整个循环
  breakLabel: for (int x = 1; x < 100; x++) {
    for (int y = 1; y < 10; y++) {
      print("x=$x, y=$y");
      if (x == 1 && y == 3) {
        break breakLabel;
      }
    }
  }

  // continue label 跳到标签出开始，结束本次循环，进入下一次循环
  continueLabel: for (var m = 1; m < 3; m++) {
    for (var n = 0; n < 10; n++) {
      print("m=$m, n=$n");
      if (n==2) {
        continue continueLabel;
      }
    }
  }
}