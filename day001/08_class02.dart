/**
 * <p>program: dart-learn</p>
 * <p>Description:构造方法 :基础构造，命名构造，常量构造，工厂构造,初始化列表</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */

void main() {
  // 默认空构造方法 构造方法不能重载 （可使用命名构造方法：类名.方法）
//  var person = new Person();
//  person.age = 12;
//  person.name = 'Tom';
//  person.work();
  var person = new Person('Tom', 12);
  person.info();
  // 命名构造方法(创建多个构造方法)
  var personWithName = new Person.withName('Tome');
  personWithName.getName();

  // 使用常量声明对象，常量构造方法
  // 如果类是不可变状态，可以把对象定义为编译时常量
  // 使用const 声明构造方法，并且 所有变量都为final
  const constPerson = ConstPerson('lucy',20);
  constPerson.info();

  // 工厂构造方法类是与设计模式的工厂模式
  // 在构造方法前添加关键字factory 实现一个工厂构造方法
  // 在工厂构造方法中可以返回对象
  var log = new Logger('Tom');

  // 初始化列表会在构造方法体执行前执行
  // 使用逗号分隔初始化表达式
  // 初始化列表常用与设置final变量的值（普通值也可是定）
  Map map = new Map();
  map['name'] = 'wyl';
  map['age'] = 18;
  map['gender'] = 'male';
  var intList = new initList.withMap(map);
  intList.info();
}

class Person {
  String name;
  int age;

  // 空构造，默认
//  Person();

  Person(this.name, this.age);

  Person.withName(this.name);

  // _ private级别
  Person._withAge(this.age);

  void info() {
    print("$name : $age");
  }

  void getName() {
    print(name);
  }
}

class ConstPerson{
  final String name;

  final int age;

  const ConstPerson(this.name, this.age);

  void info() {
    print("$name : $age");
  }
}

class Logger{
  String name;

  // 可在构造方法里面直接返回
  factory Logger(String name){
    return Logger._internal('dart');
  }

  Logger._internal(this.name);

  void log(String msg){
    print(msg);
  }
}

class initList{
  String name;
  int age;

  final String gender;

  initList.withMap(Map map):gender= map['gender'],name = map['name']{
    this.age = map['age'];
  }

  void info(){
    print("$name : $age : $gender");
  }
}