/**
 * <p>program: dart-learn</p>
 * <p>Description: 闭包处理</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */

void main() {

  /****
   * 1.闭包是一个方法（对象）
   * 2.闭包定义在其他方法内部的方法
   * 3.闭包能够访问外部方法内的局部变量，并持有其状体
   */

  // 获取闭包
  var func = closureA();
  // 执行闭包
  func(); // 0
  func(); // 1
  func(); // 2
  func(); // 3
}

// 声明一个无返回值类型、无参数的函数
closureA() {
  int count = 0;

//  printCont(){
//    print(count++);
//  }
//
//  return printCont;

  // 将闭包返回 匿名方法 推荐
  return () {
    print(count++);
  };
}
