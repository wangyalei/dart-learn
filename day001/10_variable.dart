/**
 * <p>program: dart-learn</p>
 * <p>Description:变量声明 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
void main() {

  /****
   * 1.var如果没有初始值，可以变成任何类型,如果有初始值，那么其类型将会被锁定;
   * 2.Object动态任意类型，编译阶段检查类型
   * 3.dynamic动态任意类型，编译阶段不检查类型
   */

  //  var  等类型声明的变量，再次赋值其他类型会报错，如下：
  //  var s = 's1';
  //  s = 1;

  // 这些，也完全可以使用 var 来处理，让编译器自动推断；
  var s = 's1'; // type String
  var i = 2;    // type int
  var f = 2.2;  // type double

  // 如果使用 dynamic 申明的变量，则可以进行此操作
  dynamic ss = 's1';
  print(ss);   // 值为 s1
  ss = 1;
  print(ss);   // 值为 1

  // 使用is 关键词来判断类型
  var str = '1234';
  print(str is String);

}