/**
 * <p>program: dart-learn</p>
 * <p>Description: 函数</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/17
 */
main(){

  /****
   * 1.Dart 是完全面向对象的语言，即使是函数也是对象，所以函数也可以被声明成变量，
   * 或者作为另一个函数的参数。也可以像调用函数一样调用类的实例变量。
   * 2.建议函数都指明返回值类型，虽然不写类型也能执行
   */

  //is  is!操作符判断对象是否为指定类型，如num、String等
  assert(add is Function);

  var n = add(200, 100);
  print(n);

  var m = minus(200, 100);
  print(m);
  // 方法对象
  Function z = minus;
  print(z(100,200));

  // 可选参数(基于位置)
  func(1, 2, 3);
  func(4, 5);
  func(6);

  // 动态参数(基于命名的可选参数) 推荐
  func2(2);
  func2(2,a:3);
  func2(2,b:4,a:3);

  // 动态参数(默认参数)推荐
  fun3(1);
  fun3(1,y:2);

  // 方法参数带入
  var list = ["1","2","3","4"];
  print(listTimes(list, times));

  // 匿名方法 与js 定义一样
  var andyFunc = ()  {
    print("匿方法");
  };
  andyFunc();

  // 不推荐使用
  ((){
    print("匿方法2");
  })();
  var list2 = ["1","2","3","4"];
  // 匿名方法参数带入
  print(listTimes2(list2, (str) => str + "匿名方法")) ;
}

int add(int x, int y) {
  return x + y;
}

// 函数返回值类型为dynamic '=>' 等价于 '{return ;}'
minus(int x, int y) => x - y;

// 可选参数(基于位置)
func(int a, [int b, int c]) {
  print("a is $a");
  print("b is $b");
  print("c is $c");
}

// 动态参数(基于命名的可选参数)
func2(int x, {int a, int b}) {
  print("x is $x");
  print("a is $a");
  print("b is $b");
}

// 动态参数(默认参数)
fun3(int x, {int y = 10}) {
  print("x is $x");
  print("y is $y");
}

bool isBool(int a, int b) {
  return a == b;
}

// dynamic return type（动态返回类型的定义）
isBoolThree(int a, int b) {
  if (a < b) {
    return a;
  } else if (a == b) {
    return true;
  } else if (a > b) {
    return "min:${b}";
  }
}

// 定义一个方法参数
List<String> listTimes(List list ,String times(str)){
    for(var i = 0; i < list.length; i++){
      list[i] = times(list[i]);
    }
    return list;
}

String times(str){
  return str + 'times';
}

// 定义一个方法参数
List<String> listTimes2(List list ,String times2(str)){
  for(var i = 0; i < list.length; i++){
    list[i] = times2(list[i]);
  }
  return list;
}