
## dart- 简介

- Dart是Google发布的一门开源编程语言
- Dart初期目标是成为下一代web开发语言
- Dart目前已经成为全平台开发语言
- Dart是一门面向对象的编程语言

## dart001- 基础篇

1. 注释使用
    - 单行/多行 注释
2.  内置基础数据类型使用（str,num,list,map,runes）
    - 字符串操作（String）
    - 数字操作（num）
    - 集合操作（List，Map）
    - 常量操作（final，const）
3. 条件控制语句
    - IF ELSE
    - 条件表达式
    - Switch Case
4. 循环控制
    - for
    - while
    - do ..while
    - break
    - continue
    - 高级用法（breakLabel，continueLabel）
5. 闭包
6. 函数
    - 函数定义
    - 可选参数（基于位置）
    - 命名参数
7. 异常处理
    - try ..on
    - try ..catch
    - try ..finally
    - 自定义异常
8. 继承
    - extends
    - abstract
    - implements
    - 混入（mixin）
9. 类
    - 定义类
    - 类成员可见性
    - 属性
    - 构造函数（基础构造，命名构造，常量构造，工厂构造，初始化列表）
    - 成员变量赋值
10. 变量的声明
    - var、Object 或 dynamic 

## dart002- 高级篇
更新中


## dart003- 应用实践

更新中

## dart004- 设计模式
