/**
抽象工厂模式 (Abstract Factory Pattern)

意图：提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类。
主要解决：主要解决接口选择的问题。
何时使用：系统的产品有多于一个的产品族，而系统只消费其中某一族的产品。
如何解决：在一个产品族里面，定义多个产品。
*/
main(List<String> args) {
  //获取形状工厂
  AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");

  //获取形状为 Circle 的对象
  Shape shape1 = shapeFactory.getShape("CIRCLE");
  shape1.draw();
  //获取形状为 Rectangle 的对象
  Shape shape2 = shapeFactory.getShape("RECTANGLE");
  shape2.draw();
  //获取形状为 Square 的对象
  Shape shape3 = shapeFactory.getShape("SQUARE");
  shape3.draw();

  //获取颜色工厂
  AbstractFactory colorFactory = FactoryProducer.getFactory("COLOR");

  //获取颜色为 Red 的对象
  Color color1 = colorFactory.getColor("RED");
  color1.fill();

  //获取颜色为 Green 的对象
  Color color2 = colorFactory.getColor("GREEN");
  color2.fill();

  //获取颜色为 Blue 的对象
  Color color3 = colorFactory.getColor("BLUE");
  color3.fill();
}

//////////////////////////////////////////////////////////////////

///
/// 创建一个接口
///
abstract class Shape {
  void draw();
}

///
/// 创建实现接口的实体类
///
class Rectangle implements Shape {
  @override
  void draw() {
    print("Inside Rectangle::draw() method.");
  }
}

class Square implements Shape {
  @override
  void draw() {
    print("Inside Square::draw() method.");
  }
}

class Circle implements Shape {
  @override
  void draw() {
    print("Inside Circle::draw() method.");
  }
}

///
/// 为颜色创建一个接口
///
abstract class Color {
  void fill();
}

///
/// 创建实现接口的实体类
///
class Red implements Color {
  @override
  void fill() {
    print("Inside Red::fill() method.");
  }
}

class Green implements Color {
  @override
  void fill() {
    print("Inside Green::fill() method.");
  }
}

class Blue implements Color {
  @override
  void fill() {
    print("Inside Blue::fill() method.");
  }
}

///
/// 为 Color 和 Shape 对象创建抽象类来获取工厂
///
abstract class AbstractFactory {
  Color getColor(String color);
  Shape getShape(String shape);
}

///
/// 创建扩展了 AbstractFactory 的工厂类，基于给定的信息生成实体类的对象
///
class ShapeFactory extends AbstractFactory {
  @override
  Shape getShape(String shapeType) {
    switch (shapeType?.toUpperCase()) {
      case "CIRCLE":
        return Circle();
        break;
      case "RECTANGLE":
        return Rectangle();
        break;
      case "SQUARE":
        return Square();
        break;
      default:
        return null;
    }
  }

  @override
  Color getColor(String color) {
    return null;
  }
}

class ColorFactory extends AbstractFactory {
  @override
  Shape getShape(String shapeType) {
    return null;
  }

  @override
  Color getColor(String color) {
    switch (color?.toUpperCase()) {
      case "RED":
        return Red();
        break;
      case "GREEN":
        return Green();
        break;
      case "BLUE":
        return Blue();
        break;
      default:
        return null;
    }
  }
}

///
/// 创建一个工厂创造器/生成器类，通过传递形状或颜色信息来获取工厂
///
class FactoryProducer {
  static AbstractFactory getFactory(String choice) {
    if (choice == "SHAPE") {
      return ShapeFactory();
    } else if (choice == "COLOR") {
      return ColorFactory();
    }
    return null;
  }
}
