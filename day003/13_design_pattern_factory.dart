/**
工厂模式(Factory Pattern)

意图：定义一个创建对象的接口，让其子类自己决定实例化哪一个工厂类，工厂模式使其创建过程延迟到子类进行。
主要解决：主要解决接口选择的问题。
何时使用：我们明确地计划不同条件下创建不同实例时。
如何解决：让其子类实现工厂接口，返回的也是一个抽象的产品。
*/
main(List<String> args) {
  var shapeFactory = ShapeFactory();
  //获取 Circle 的对象，并调用它的 draw 方法
  Shape shape1 = shapeFactory.getShape("CIRCLE");
  //调用 Circle 的 draw 方法
  shape1.draw();

  //获取 Rectangle 的对象，并调用它的 draw 方法
  Shape shape2 = shapeFactory.getShape("RECTANGLE");
  //调用 Rectangle 的 draw 方法
  shape2.draw();

  //获取 Square 的对象，并调用它的 draw 方法
  Shape shape3 = shapeFactory.getShape("SQUARE");
  //调用 Square 的 draw 方法
  shape3.draw();
}

//////////////////////////////////////////////////////////////////

///
/// 创建一个接口
///
abstract class Shape {
  void draw();
}

///
/// 创建实现接口的实体类
///
class Rectangle implements Shape {
  @override
  void draw() {
    print("Inside Rectangle::draw() method.");
  }
}

class Square implements Shape {
  @override
  void draw() {
    print("Inside Square::draw() method.");
  }
}

class Circle implements Shape {
  @override
  void draw() {
    print("Inside Circle::draw() method.");
  }
}

///
/// 创建一个工厂
///
class ShapeFactory {
  //使用 getShape 方法获取形状类型的对象
  Shape getShape(String shapeType) {
    switch (shapeType?.toUpperCase()) {
      case "CIRCLE":
        return Circle();
        break;
      case "RECTANGLE":
        return Rectangle();
        break;
      case "SQUARE":
        return Square();
        break;
      default:
        return null;
    }
  }
}
