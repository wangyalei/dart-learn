/**
 * <p>program: dart-learn</p>
 * <p>Description: 异步</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/22 0:07
 */
//Dart提供了类似ES7中的async await等异步操作，这种异步操作在Flutter开发中会经常遇到，
//比如网络或其他IO操作，文件选择等都需要用到异步的知识。 async和await往往是成对出现的，
//如果一个方法中有耗时的操作，你需要将这个方法设置成async，并给其中的耗时操作加上await关键字，
//如果这个方法有返回值，你需要将返回值塞到Future中并返回，如下代码所示：
//main() {
//  getNetData().then((str) {
//    print(str);
//  });
//}
//
//Future<String> getNetData() async {
//  http.Response res = await http.get("http://www.baidu.com");
//  return res.body;
//}