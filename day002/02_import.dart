/**
 * <p>program: dart-learn</p>
 * <p>Description: 库引用</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/22 23:38
 */


// Dart中任何文件都是一个库，即使你没有用关键字library声明

// dart:前缀表示Dart的标准库，如dart:io、dart:html
import 'dart:math';

// 可以用相对路径或绝对路径的dart文件来引用
import '../lib/student.dart';

// 当各个库有命名冲突的时候，可以使用as关键字来使用命名空间
import '../lib/student.dart' as Stu;

//// Pub包管理系统中有很多功能强大、实用的库，可以使用前缀 package:
// import 'package:args/args.dart';

// http://www.cndartlang.com/696.html

void main(){

  Student student = Student("dart",2);
  print(student.age);

  Stu.Student stuS = Stu.Student("dartAs",1);
  print(student.name);
}