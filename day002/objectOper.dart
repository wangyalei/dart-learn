/**
 * <p>program: dart-learn</p>
 * <p>Description:对象操作符: 对象条件运算符, 类型转换 ,级联操作,call 处理</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/20 23:51
 */
void main() {
  Person person = new Person();
  // 对象条件运算符
  person?.info();
  // 类型转换
  dynamic person1 = "";
  person1 = new Person();
  // is  is!
  if (person1 is Person) {
    person1.info();
  }

  // 级联操作.. 与 builder类似
  new Person()..name = 'wyl'
         ..age = 18
         ..info();

  // 调用call
  person.name = 'li';
  person.age = 101;
//  person();
  print(person('li',101));
}

class Person {
  String name;
  int age;

  void info() {
    print("$name : $age");
  }
  
  // 实现call方法
//  void call(){
//    print('call method :$name : $age');
//  }

  String call(String name,int age){
    return 'call method :$name : $age';
  }
}
