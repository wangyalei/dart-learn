/**
 * <p>program: dart-learn</p>
 * <p>Description: 计算属性</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/20 23:42
 */
void main() {
  var rect = new Rectangle();
  rect.width = 10;
  rect.height = 20;
  // 普通方法调用
  print(rect.area());

  // 计算属性
  print(rect.areaP);
  rect.areaP = 20;
  print(rect.width);
}

class Rectangle {
  num width, height;

  // 普通的写法
  num area() {
    return width * height;
  }

  // 把面积写出计算属性

  // get
  num get areaP => width * height;

  // set
  set areaP(value) {
    width = value ~/ 2;
  }
}
