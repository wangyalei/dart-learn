/**
 * <p>program: dart-learn</p>
 * <p>Description: Mixins类似于多继承，是在多类继承中重用一个类代码的方式 用 with调用 与js中的minix类似</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/21 23:56
 */

void main() {
  C c = new C();
  c.a();  // A's a()
  c.b();  // B's b()
}

// 使用with关键字，表示类C是由类A和类B混合而构成
// Mixins类似于多继承，是在多类继承中重用一个类代码的方式 用 with调用 与vue中的minix类似
// with 可以重用多个类 with class1，class2，。。。
// 使用Mixins 必须先使用extends，
// 作为Mixins不能有显示的构造方法，只能默认继承Object方法

// 以下完整写法  c extends A with B
class C = A with B;

class A {
  a() {
    print("A's a()");
  }
}
class B {
  b() {
    print("B's b()");
  }
}


