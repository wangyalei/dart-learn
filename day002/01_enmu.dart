/**
 * <p>program: dart-learn</p>
 * <p>Description: 枚举</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: wylCloud.com</p>
 *
 * @author wyl (764198025@qq.com)
 * @version 1.0
 * @date Create in 2020/6/22 23:38
 */

enum Season{
  spring,
  summer,
  autumn,
  winter
}

void main(){
  // index属性
  print(Season.spring.index);
  print(Season.spring);
}